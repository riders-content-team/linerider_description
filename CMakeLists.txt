cmake_minimum_required(VERSION 3.0.2)
project(linerider_description)

## Compile as C++11, supported in ROS Kinetic and newer
add_compile_options(-std=c++11)

find_package(catkin REQUIRED COMPONENTS
  roscpp
  rospy
  std_msgs
  message_generation
  gazebo_ros
)

find_package(gazebo REQUIRED)

link_directories(${GAZEBO_LIBRARY_DIRS})

add_service_files(
  FILES
  CameraSensor.srv
  LineriderControl.srv
)

generate_messages(
  DEPENDENCIES std_msgs geometry_msgs
)

catkin_package(
  CATKIN_DEPENDS
    roscpp
    gazebo_ros
    message_runtime
    std_msgs
)

include_directories(${Boost_INCLUDE_DIR} ${catkin_INCLUDE_DIRS} ${GAZEBO_INCLUDE_DIRS})

install(DIRECTORY meshes rviz urdf
  DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}
)


# skid steer plugin
add_library(RidersSkidSteer plugins/RidersSkidSteer.cc)
add_dependencies(RidersSkidSteer ${catkin_EXPORTED_TARGETS})
target_link_libraries(RidersSkidSteer ${catkin_LIBRARIES} ${GAZEBO_LIBRARIES})

# camera sensor plugin
add_library(CameraSensorPlugin plugins/CameraSensorPlugin.cc)
add_dependencies(CameraSensorPlugin ${catkin_EXPORTED_TARGETS})
target_link_libraries(CameraSensorPlugin ${catkin_LIBRARIES} ${GAZEBO_LIBRARIES})
