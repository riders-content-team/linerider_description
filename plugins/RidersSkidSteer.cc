#include "RidersSkidSteer.hh"

using namespace gazebo;
// Register this plugin with the simulator
GZ_REGISTER_MODEL_PLUGIN(RidersSkidSteer)

// Constructor
RidersSkidSteer::RidersSkidSteer() : ModelPlugin()
{
}

// Destructor
RidersSkidSteer::~RidersSkidSteer()
{
}

void RidersSkidSteer::Load(physics::ModelPtr model, sdf::ElementPtr _sdf)
{
  this->model = model;
  this->world = this->model->GetWorld();
  this->world->SetPaused(true);

  // Get robot namespace
  if (!_sdf->HasElement("robotNamespace"))
  {
    this->robot_namespace = "robot";
  } else {
    this->robot_namespace = _sdf->Get<std::string>("robotNamespace");
  }

  //Get wheel joints
  if (!_sdf->HasElement("leftFrontJoint"))
  {
    this->left_front_joint_name = "left_front_joint";
  } else {
    this->left_front_joint_name = _sdf->Get<std::string>("leftFrontJoint");
  }

  if (!_sdf->HasElement("rightFrontJoint"))
  {
    this->right_front_joint_name = "right_front_joint";
  } else {
    this->right_front_joint_name = _sdf->Get<std::string>("rightFrontJoint");
  }

  if (!_sdf->HasElement("leftRearJoint"))
  {
    this->left_rear_joint_name = "left_rear_joint";
  } else {
    this->left_rear_joint_name = _sdf->Get<std::string>("leftRearJoint");
  }

  if (!_sdf->HasElement("rightRearJoint"))
  {
    this->right_rear_joint_name = "right_rear_joint";
  } else {
    this->right_rear_joint_name = _sdf->Get<std::string>("rightRearJoint");
  }

  if (!_sdf->HasElement("wheelSeparation"))
  {
    this->wheel_separation = 0.4;
  } else {
    this->wheel_separation = _sdf->Get<double>("wheelSeparation");
  }

  if (!_sdf->HasElement("wheelDiameter"))
  {
    this->wheel_diameter = 0.15;
  } else {
    this->wheel_diameter = _sdf->Get<double>("wheelDiameter");
  }

  if (!_sdf->HasElement("torque"))
  {
    this->torque = 5;
  } else {
    this->torque = _sdf->Get<double>("torque");
  }

  if (!_sdf->HasElement("cmd_vel"))
  {
    this->command_topic = "right_rear_joint";
  } else {
    this->command_topic = _sdf->Get<std::string>("cmd_vel");
  }

  if (!_sdf->HasElement("commandTopic"))
  {
    this->command_topic = "cmd_vel";
  } else {
    this->command_topic = _sdf->Get<std::string>("commandTopic");
  }

  if (!_sdf->HasElement("odometryFrame"))
  {
    this->odometry_frame = "odom";
  } else {
    this->odometry_frame = _sdf->Get<std::string>("odometryFrame");
  }

  if (!_sdf->HasElement("odometryTopic"))
  {
    this->odometry_topic = "odom";
  } else {
    this->odometry_topic = _sdf->Get<std::string>("odometryTopic");
  }

  if (!_sdf->HasElement("robotBaseFrame"))
  {
    this->robot_base_frame = "base_footprint";
  } else {
    this->robot_base_frame = _sdf->Get<std::string>("robotBaseFrame");
  }

  if (!_sdf->HasElement("updateRate"))
  {
    this->update_rate = 100.0;
  } else {
    this->update_rate = _sdf->Get<double>("updateRate");
  }

  // Initialize update rate stuff
  if (this->update_rate > 0.0) {
    this->update_period = 1.0 / this->update_rate;
  } else {
    this->update_period = 0.0;
  }

  this->last_update_time = this->world->SimTime().Double();

  // Initialize velocity stuff
  this->wheel_speed[RIGHT_FRONT] = 0;
  this->wheel_speed[LEFT_FRONT] = 0;
  this->wheel_speed[RIGHT_REAR] = 0;
  this->wheel_speed[LEFT_REAR] = 0;

  this->x = 0;
  this->rot = 0;

  this->joints[LEFT_FRONT] = this->model->GetJoint(this->left_front_joint_name);
  this->joints[RIGHT_FRONT] = this->model->GetJoint(this->right_front_joint_name);
  this->joints[LEFT_REAR] = this->model->GetJoint(this->left_rear_joint_name);
  this->joints[RIGHT_REAR] = this->model->GetJoint(this->right_rear_joint_name);

  if (!this->joints[LEFT_FRONT]) {
    char error[200];
    snprintf(error, 200,
        "GazeboRosSkidSteerDrive Plugin (ns = %s) couldn't get left front hinge joint named \"%s\"",
        this->robot_namespace.c_str(), this->left_front_joint_name.c_str());
    gzthrow(error);
  }

  if (!this->joints[RIGHT_FRONT]) {
    char error[200];
    snprintf(error, 200,
        "GazeboRosSkidSteerDrive Plugin (ns = %s) couldn't get right front hinge joint named \"%s\"",
        this->robot_namespace.c_str(), this->right_front_joint_name.c_str());
    gzthrow(error);
  }

  if (!this->joints[LEFT_REAR]) {
    char error[200];
    snprintf(error, 200,
     "GazeboRosSkidSteerDrive Plugin (ns = %s) couldn't get left rear hinge joint named \"%s\"",
     this->robot_namespace.c_str(), this->left_rear_joint_name.c_str());
    gzthrow(error);
  }

  if (!this->joints[RIGHT_REAR]) {
    char error[200];
    snprintf(error, 200,
     "GazeboRosSkidSteerDrive Plugin (ns = %s) couldn't get right rear hinge joint named \"%s\"",
     this->robot_namespace.c_str(), this->right_rear_joint_name.c_str());
    gzthrow(error);
  }

  this->joints[LEFT_FRONT]->SetParam("fmax", 0, torque);
  this->joints[RIGHT_FRONT]->SetParam("fmax", 0, torque);
  this->joints[LEFT_REAR]->SetParam("fmax", 0, torque);
  this->joints[RIGHT_REAR]->SetParam("fmax", 0, torque);

  // Initialize ros, if it has not already bee initialized.
  if (!ros::isInitialized())
  {
    int argc = 0;
    char **argv = NULL;
    ros::init(argc, argv, this->robot_namespace,
      ros::init_options::NoSigintHandler);
  }

  // Create our ROS node. This acts in a similar manner to
  // the Gazebo node
  this->rosNode.reset(new ros::NodeHandle(this->robot_namespace));

  ros::SubscribeOptions so =
    ros::SubscribeOptions::create<geometry_msgs::Twist>(
      this->command_topic,
      1,
      std::bind(&RidersSkidSteer::cmdVelCallback, this, std::placeholders::_1),
      ros::VoidConstPtr(),
      NULL);

  //this->cmd_vel_subscriber = this->rosNode->subscribe(so);

  //Service init
  ros::AdvertiseServiceOptions rosrider_control =
  ros::AdvertiseServiceOptions::create<linerider_description::LineriderControl>(
    "linerider_control",
      boost::bind(&RidersSkidSteer::ControlCallback, this, _1, _2),
        ros::VoidConstPtr(), NULL);

  // Advertise Service
  this->control_service = this->rosNode->advertiseService(rosrider_control);

  this->odometry_publisher = this->rosNode->advertise<nav_msgs::Odometry>(this->odometry_topic, 1);

  //this->worldConnection = event::Events::ConnectWorldUpdateBegin(
  //        std::bind(&RidersSkidSteer::Update, this));
}

void RidersSkidSteer::cmdVelCallback(const geometry_msgs::Twist::ConstPtr& cmd_msg)
{
  this->x = cmd_msg->linear.x;
  this->rot = cmd_msg->angular.z;
}

bool RidersSkidSteer::ControlCallback(
  linerider_description::LineriderControl::Request &req,
    linerider_description::LineriderControl::Response &res)
{
  x = req.lx;
  rot = req.az;
  uint64_t step_size = req.step_size;

  this->getWheelVelocities();
  this->joints[LEFT_FRONT]->SetParam("vel", 0, this->wheel_speed[LEFT_FRONT] / (this->wheel_diameter / 2.0));
  this->joints[RIGHT_FRONT]->SetParam("vel", 0, this->wheel_speed[RIGHT_FRONT] / (this->wheel_diameter / 2.0));
  this->joints[LEFT_REAR]->SetParam("vel", 0, this->wheel_speed[LEFT_REAR] / (this->wheel_diameter / 2.0));
  this->joints[RIGHT_REAR]->SetParam("vel", 0, this->wheel_speed[RIGHT_REAR] / (this->wheel_diameter / 2.0));

  this->world->Step(step_size);

  res.success = true;

  return true;
}

void RidersSkidSteer::Update()
{
  double current_time = this->world->SimTime().Double();
  double seconds_since_last_update = current_time - this->last_update_time;

  if (seconds_since_last_update > this->update_period)
  {
    this->getWheelVelocities();
    this->joints[LEFT_FRONT]->SetParam("vel", 0, this->wheel_speed[LEFT_FRONT] / (this->wheel_diameter / 2.0));
    this->joints[RIGHT_FRONT]->SetParam("vel", 0, this->wheel_speed[RIGHT_FRONT] / (this->wheel_diameter / 2.0));
    this->joints[LEFT_REAR]->SetParam("vel", 0, this->wheel_speed[LEFT_REAR] / (this->wheel_diameter / 2.0));
    this->joints[RIGHT_REAR]->SetParam("vel", 0, this->wheel_speed[RIGHT_REAR] / (this->wheel_diameter / 2.0));

    this->last_update_time = this->world->SimTime().Double();
  }
}

void RidersSkidSteer::getWheelVelocities()
{
  double vr = this->x;
  double va = this->rot;

  this->wheel_speed[RIGHT_FRONT] = vr + va * this->wheel_separation / 2.0;
  this->wheel_speed[RIGHT_REAR] = vr + va * this->wheel_separation / 2.0;

  this->wheel_speed[LEFT_FRONT] = vr - va * this->wheel_separation / 2.0;
  this->wheel_speed[LEFT_REAR] = vr - va * this->wheel_separation / 2.0;
}

void RidersSkidSteer::publishOdometry()
{
  nav_msgs::Odometry odom;
  ros::Time current_time = ros::Time::now();
  std::string odom_frame = this->robot_base_frame;

  ignition::math::Pose3d pose = this->model->WorldPose();

  odom.pose.pose.position.x = pose.Pos().X();
  odom.pose.pose.position.y = pose.Pos().Y();

  odom.pose.pose.orientation.x = pose.Rot().X();
  odom.pose.pose.orientation.y = pose.Rot().Y();
  odom.pose.pose.orientation.z = pose.Rot().Z();
  odom.pose.pose.orientation.w = pose.Rot().W();
  odom.pose.covariance[0] = 0;
  odom.pose.covariance[7] = 0;
  odom.pose.covariance[14] = 1000000000000.0;
  odom.pose.covariance[21] = 1000000000000.0;
  odom.pose.covariance[28] = 1000000000000.0;
  odom.pose.covariance[35] = 0;

  ignition::math::Vector3d linear = this->model->WorldLinearVel();
  odom.twist.twist.angular.z = this->model->WorldAngularVel().Z();

  float yaw = pose.Rot().Yaw();
  odom.twist.twist.linear.x = cosf(yaw) * linear.X() + sinf(yaw) * linear.Y();
  odom.twist.twist.linear.y = cosf(yaw) * linear.Y() - sinf(yaw) * linear.X();
  odom.twist.covariance[0] = 0;
  odom.twist.covariance[7] = 0;
  odom.twist.covariance[14] = 1000000000000.0;
  odom.twist.covariance[21] = 1000000000000.0;
  odom.twist.covariance[28] = 1000000000000.0;
  odom.twist.covariance[35] = 0;

  odom.header.stamp = current_time;
  odom.header.frame_id = odom_frame;
  odom.child_frame_id = this->robot_base_frame;

  this->odometry_publisher.publish(odom);

}
