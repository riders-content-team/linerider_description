#ifndef RIDERS_SKID_STEER_HH
#define RIDERS_SKID_STEER_HH

#include "gazebo/common/common.hh"
#include "ros/ros.h"
#include "gazebo/physics/PhysicsTypes.hh"
#include "gazebo/physics/MultiRayShape.hh"
#include "gazebo/physics/World.hh"
#include "gazebo/physics/Model.hh"
#include "gazebo/physics/Joint.hh"
#include "gazebo/physics/PhysicsIface.hh"

#include "linerider_description/LineriderControl.h"

#include <geometry_msgs/Twist.h>
#include <nav_msgs/Odometry.h>



namespace gazebo
{
  class GAZEBO_VISIBLE RidersSkidSteer : public ModelPlugin
  {
    // brief Constructor
    public: RidersSkidSteer();

    // brief Destructor
    public: ~RidersSkidSteer();

    // brief Load the plugin
    // param take in SDF root element
    public: virtual void Load(physics::ModelPtr _model, sdf::ElementPtr _sdf);

    public: virtual void cmdVelCallback(const geometry_msgs::Twist::ConstPtr& cmd_msg);
    public: bool ControlCallback(linerider_description::LineriderControl::Request &req,
        linerider_description::LineriderControl::Response &res);

    private: virtual void Update();
    private: virtual void getWheelVelocities();
    private: virtual void publishOdometry();

    private: physics::ModelPtr model;
    private: physics::WorldPtr world;

    // Connection to World Update events.
    private: event::ConnectionPtr worldConnection;

    private: std::string left_front_joint_name, right_front_joint_name,
      left_rear_joint_name, right_rear_joint_name, command_topic, odometry_topic,
        odometry_frame, robot_base_frame, robot_namespace;

    private: double wheel_separation, wheel_diameter, torque, update_rate,
      update_period, last_update_time, x, rot;

    private: double wheel_speed[4];
    private: physics::JointPtr joints[4];

    private: enum wheels
      {
        RIGHT_FRONT = 0,
        LEFT_FRONT = 1,
        RIGHT_REAR = 2,
        LEFT_REAR = 3
      };

    protected: std::unique_ptr<ros::NodeHandle> rosNode;
    private: ros::Subscriber cmd_vel_subscriber;
    private: ros::Publisher odometry_publisher;

    private: ros::ServiceServer control_service;
  };
}
#endif
